# GPS-editor
GPS data display and processing application that allows you to:  
:point_right:import files that contain GPS data;  
:point_right:visually display tracks;  
:point_right:edit imported data.

## How to start:
use "docker pull yuriysolodovnikk/gps-editor"   
then "docker run -p 8080:8080 yuriysolodovnikk/gps-editor"   
and finaly open the project on  "http://localhost:8080"   
